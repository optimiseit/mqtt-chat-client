# MQTT-chat-client

## Browser:
To use the mqtt-chat client in the browser we have to make a rollup build via:

```bash
npm run build
```

Then include external dependencies for axios and mqtt before including the script in the html
```html
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/mqtt@4.2.6/dist/mqtt.min.js"></script>
<script src="dist/index.min.js"></script>
```

Then start using the mqtt chat client like this:

```html
<script>
  let client = new mqttChatClient.UserChatClient();
</script>
```


## Node:

// TODO
