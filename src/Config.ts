export interface Config {
  apiBaseUrl?: string;
  mqttBaseUrl?: string;
}
