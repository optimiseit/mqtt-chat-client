export type Message = {
  type: string;
  payload?: CustomerMessage | OperatorMessage | AgentIdentity | BotHandover;
}

type CustomerMessage = {
  name: string;
  text: string;
  timestamp: number;
}

type OperatorMessage = {
  name: string;
  text: string;
  timestamp: number;
  iceServer: RTCIceServer[];
  delay: number;
  actions: Actions[];
}

type AgentIdentity = {
  name: string;
  text: string;
  chatId: number;
  bot_config: BotConfig;
  picture: string;
  transfer: boolean | number;
  greetings_message: string;
}

type BotHandover = {
  status: string;
  success: boolean;
  deptId: number;
  requestId: number;
  token: string;
  chatId: number;
  offlineWindow: string;
  oldChatId: number;
  session_rp: string;
  failureEvent: string;
}

type BotConfig = {
  frontend_hide_input: boolean;
}

type Actions = {
  name: string;
  payload: string;
}
