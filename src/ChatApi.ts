import {Config} from "./Config";
import {Message} from "./Message";
import {EventEmitter} from "./EventEmitter";
import {ApiClient} from "./ApiClient";
import {Chat} from "./models/Chat";

export abstract class ChatApi extends EventEmitter {
  public subscribedChats: Set<string> = new Set<string>();
  protected client: any = null;
  protected api: ApiClient;
  private isIot: boolean;

  protected constructor(protected config: Config = { apiBaseUrl: 'http://localhost:3000', mqttBaseUrl: 'ws://127.0.0.1:15675/ws'}) {
    super();
    this.api = new ApiClient(config.apiBaseUrl);
    this.isIot = /\.cloud$/.test(config.mqttBaseUrl) || /cloud80-pubsub\.realperson\.de$/.test(config.mqttBaseUrl);
  }

  public get isConnected(): boolean {
    return !!this.client;
  }

  private convertTopic(topic: string) {
    return this.isIot
      ? topic.replace('*', '+')
      : topic;
  }

  protected subscribeTo(chat: Chat) {
    this.client.subscribe(this.convertTopic(chat.subscriberTopic), err => {
      if (!err) {
        this.emit('join', chat.topic);
        this.subscribedChats.add(chat.topic);
      } else {
        this.emit('join_error', err);
      }
    });
  }

  protected unsubscribeFrom(chat: Chat) {
    this.client.unsubscribe(this.convertTopic(chat.subscriberTopic), err => {
      if (!err) {
        this.emit('leave', chat.topic);
        this.subscribedChats.delete(chat.topic);
      } else {
        this.emit('leave_error', err);
      }
    });
  }

  protected publishTo(chat: Chat, sender: string, message: Message) {
    this.client.publish(chat.senderTopic, JSON.stringify({
        sender,
        msg: message
      }),
      {
        qos: this.isIot ? 1 : 2,
      });
  }

  public handleMessage = (topic, buffer) => {
    const data = JSON.parse(buffer.toString());
    topic = topic.split('/').slice(0, 3).join('/');
    this.emit('message', {topic, data});
  }

  public handleError = (err) => {
    this.emit('error', err);
  }

  public handleDisconnect = () => {
    this.subscribedChats = new Set<string>();
    this.emit('disconnect');
  }

  public disconnect() {
    this.client.end();
  }
}
