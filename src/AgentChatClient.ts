import {ChatApi} from "./ChatApi";
import {Config} from "./Config";
import {Message} from "./Message";
import {connect} from "mqtt";
import {AgentChat} from "./models/AgentChat";

export class AgentChatClient extends ChatApi {
  private openChats: Map<string, AgentChat> = new Map<string, AgentChat>();

  constructor(config?: Config) {
    super(config);
  }

  public async listOpenChats(): Promise<AgentChat[]> {
    const chats = await this.api.fetchOpenChats();
    this.openChats = new Map<string, AgentChat>();
    for (let chat of chats) {
      this.openChats.set(chat.topic, chat);
    }
    return chats;
  }

  public async connect(): Promise<void> {
    return new Promise(async (resolve) => {
      if (!this.isConnected) {
        const {token} = await this.api.fetchAgentToken();
        this.client = connect(this.config.mqttBaseUrl, {username: 'guest', password: token, rejectUnauthorized: false, keepalive: 180});
        this.client.on('connect', () => {
          this.emit('connect');
          this.subscribedChats = new Set<string>();
          return resolve();
        });
        this.client.on('message', this.handleMessage);
        this.client.on('error', this.handleError);
        this.client.on('disconnect', this.handleDisconnect);
      }
      return resolve();
    });
  }

  public async joinChat(topic: string) {
    const chat = this.openChats.get(topic)
    this.subscribeTo(chat);
  }

  public sendMessageTo(topic: string, msg: Message) {
    const chat = this.openChats.get(topic);
    if (chat !== undefined) {
      this.publishTo(chat, 'agent', msg);
    }
  }

  public leaveChat(chat: AgentChat) {
    if (this.subscribedChats.size > 1) {
      this.unsubscribeFrom(chat);
    }
  }

  public endMqtt() {
    this.client.end();
  }
}
