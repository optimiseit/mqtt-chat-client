import {Chat} from "./Chat";

export class AgentChat extends Chat {
  public get senderTopic(): string {
    return `${this.topic}/user/msg/ps`;
  }

  public get subscriberTopic(): string {
    return `${this.topic}/agent/msg/*`;
  }
}
