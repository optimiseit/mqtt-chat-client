import {Chat} from "./Chat";
import {ChatSessionResponse} from "../ApiClient";

export class UserChat extends Chat {
  public token: string;

  constructor(chatSessionResponse: ChatSessionResponse) {
    super(chatSessionResponse);
    this.token = chatSessionResponse.token;
  }
  public get senderTopic(): string {
    return `${this.topic}/agent/msg/ps`;
  }

  public get subscriberTopic(): string {
    return `${this.topic}/user/msg/*`;
  }
}
