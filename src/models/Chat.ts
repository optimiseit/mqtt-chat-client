import {ChatResponse, ChatSessionResponse} from "../ApiClient";

export abstract class Chat {
  public topic: string;

  constructor(chatSessionResponse: ChatSessionResponse | ChatResponse) {
    this.topic = chatSessionResponse.topic;
  }

  abstract get subscriberTopic(): string;
  abstract get senderTopic(): string;
}
