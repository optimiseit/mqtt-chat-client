import {Config} from "./Config";
import {Message} from "./Message";
import {connect} from 'mqtt';
import {ChatApi} from "./ChatApi";
import {UserChat} from "./models/UserChat";

export class UserChatClient extends ChatApi {
  private chat: UserChat;

  constructor(config?: Config) {
    super(config);
  }

  public async startChat(chatId: string) {
    const chat: UserChat = await this.api.initializeChatSession(chatId);
    this.client = connect(this.config.mqttBaseUrl, {username: 'guest', password: chat.token, keepalive: 180});
    this.client.on('connect', async () => {
      this.subscribeTo(chat);
      this.chat = chat;
    });
    this.client.on('message', this.handleMessage);
    this.client.on('error', this.handleError);
    this.client.on('disconnect', this.handleDisconnect);
  }

  public sendMessage(msg: Message) {
    this.publishTo(this.chat, 'client', msg);
  }

  public stopChat() {
    this.disconnect();
  }
}
