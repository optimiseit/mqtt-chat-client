import axios from 'axios';
import {UserChat} from './models/UserChat';
import {AgentChat} from "./models/AgentChat";

export interface ChatResponse {
  topic: string;
}

export interface ChatSessionResponse extends ChatResponse {
  token: string;
}

export interface TokenResponse {
  token: string;
}

export class ApiClient {
  constructor(private baseUrl: string) {
  }

  public async fetchOpenChats(): Promise<AgentChat[]> {
    const {data: chats} = await axios.get(`${this.baseUrl}/chats`);
    return chats.map(chat => new AgentChat(chat));
  }

  public async fetchAgentToken(): Promise<TokenResponse> {
    const {data: tokenResponse} = await axios.get(`${this.baseUrl}/token`);
    return {
      token: tokenResponse.token
    }
  }

  public async initializeChatSession(chatId: string): Promise<UserChat> {
    const {data: chatSession} = await axios.post(`${this.baseUrl}/chat`, { chatId }, { headers: { 'Content-type': 'application/json' } });
    return new UserChat(chatSession);
  }
}
