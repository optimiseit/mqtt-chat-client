import typescript from '@rollup/plugin-typescript';
import dts from 'rollup-plugin-dts';

const config = [{
  input: `src/index.ts`,
  output: [
    {
      dir: 'dist', format: 'cjs', sourcemap: true
    }
  ],
  watch: {
    include: 'src/**',
  },
  plugins: [
    typescript()
  ]
}, {
  input: 'dist/index.d.ts',
  output: [
    {
      file: 'dist/index.d.ts', format: 'cjs'
    }
  ],
  plugins: [
    dts()
  ]
}];

export default config;
