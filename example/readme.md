# Examples

- install example dependencies
```bash
npm i
```

- start up rabbitmq
```bash
docker-compose up rabbitmq
```

- start up api server
```bash
node api.js
```

## nodejs example

- start client process (in this example we need to start all clients first for the agent process to list all available clients)
```bash
node nodejs/client.js
```

- start agent process
```bash
node nodejs/agent.js
```

follow commandline instructions
