const express = require('express');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const app = express();
app.use(express.json());
const port = 3000;
const JWT_PRIVATE_KEY = fs.readFileSync('./rsa-signing-key.key');

// we don't know yet what defaults are absolutely required by the rabbitmq oauth plugin
const jwtDefaults = {
  "jti": "dfb5f6a0d8d54be1b960e5ffc996f7aa",
  "sub": "71bde130-7738-47b8-8c7d-ad98fbebce4a",
  "client_id": "guest",
  "cid": "guest",
  "azp": "guest",
  "grant_type": "password",
  "user_id": "71bde130-7738-47b8-8c7d-ad98fbebce4a",
  "origin": "uaa",
  "user_name": "guest",
  "email": "guest@example.com",
  "auth_time": Date.now(),
  "rev_sig": "d5cf8503",
  "zid": "uaa"
}

const openChats = [];

app.get('/chats', async (req, res) => {
  return res.json(openChats.map(chat => ({ topic: chat.topic })));
});

app.get('/token', async (req, res) => {
  const token = await createAgentToken();
  return res.json({
    token
  });
});

app.post('/chat', async (req, res) => {
  const chat = createChat(req.body.chatId);
  const token = await createUserToken(chat);
  openChats.push(chat);
  return res.json({
    topic: chat.topic,
    token
  });
})

app.listen(port, () => {
  console.log(`Api listening at http://localhost:${port}`)
});

function createChat(chatId) {
  const chatTopic = `tenant/department/${chatId}`;
  const chatRoutingKey = chatTopic.split('/').join('.');

  return {
    id: chatId,
    topic: chatTopic,
    routingKey: chatRoutingKey
  };
}

async function createAgentToken() {
  return await jwt.sign({
    ...jwtDefaults,
    scope: [
      `rabbitmq.write:rabbitmq/*/tenant.department.*.user.msg.ps`,
      `rabbitmq.read:rabbitmq/*/tenant.department.*.*.*.*`,
      `rabbitmq.configure:rabbitmq/*/tenant.department.*.*.*.*`
    ]
  }, JWT_PRIVATE_KEY, {
    expiresIn: 3600,
    algorithm: 'RS256',
    audience: [
      "rabbitmq",
      "rabbit_client"
    ],
    issuer: "http://localhost:3000/token"
  });
}

async function createUserToken(chat) {
  const routingKey = chat.routingKey;

  return await jwt.sign({
    ...jwtDefaults,
    scope: [
      `rabbitmq.write:rabbitmq/*/${routingKey}.agent.msg.ps`,
      `rabbitmq.read:rabbitmq/*/${routingKey}.user.*.*`,
      `rabbitmq.configure:rabbitmq/*/${routingKey}.*.*.*`
    ]
  }, JWT_PRIVATE_KEY, {
    expiresIn: 3600,
    algorithm: 'RS256',
    audience: [
      "rabbitmq",
      "rabbit_client"
    ],
    issuer: "http://localhost:3000/token"
  });
}
