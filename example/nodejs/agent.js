const prompt = require('readline-sync');
const readline = require('readline');
const {AgentChatClient} = require('../../dist');

(async () => {
  const agent = new AgentChatClient();
  let promptTimeoutHandler = null;
  let userInput = null;
  const chats = await agent.listOpenChats();
  console.info('active chats:');
  console.info('##################################');
  if (chats.length) {
    for (let chat of chats) {
      console.info('-', chat.topic);
    }
  } else {
    console.info('no active chats found.')
  }
  console.info('##################################');
  const topic = await promptChatId();
  const chat = chats.find(chat => chat.topic === topic);
  console.info(`try connecting to chat ${chat.topic}...`);
  await agent.connect();
  await agent.joinChat(chat.topic);

  agent.on('join', (topic) => {
    if (userInput) {
      userInput.close();
      userInput = null;
    }
    console.log(`subscribed to chat ${topic}`);
    promptMessage(topic);
  });

  agent.on('disconnect', () => {
    console.log('disconnected');
  });

  agent.on('message', ({topic, data}) => {
    userInput.close();
    userInput = null;
    if (data.sender !== 'agent') {
      process.stdout.clearLine(-1);
      process.stdout.cursorTo(0);
      console.log(`${data.sender} @ ${topic} : ${data.msg}`);
    }
    promptMessage(topic);
  });

  agent.on('error', (err) => {
    console.log('err', err);
  });

  async function promptChatId() {
    return prompt.question('choose topic: ');
  }

  async function promptMessage(topic) {
    clearTimeout(promptTimeoutHandler);
    userInput = readline.createInterface(process.stdin, process.stdout);
    const msg = await getUserInput('msg: ');
    agent.sendMessageTo(topic, msg);
    promptTimeoutHandler = setTimeout(() => promptMessage(topic), 10);
  }

  async function getUserInput(question) {
    return new Promise((resolve, reject) => {
      userInput.question(question, (answer) => {
        resolve(answer);
        userInput.close();
      });
    });
  }
})()
