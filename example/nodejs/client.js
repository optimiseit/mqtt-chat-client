const readline = require('readline');
const {UserChatClient} = require('../../dist');
const {nanoid} = require('nanoid');

(async () => {
  const client = new UserChatClient();

  let promptTimeoutHandler = null;
  let userInput = null;

  await client.startChat(nanoid(8));
  client.on('join', () => {
    if (userInput) {
      userInput.close();
      userInput = null;
    }
    promptMessage();
  });

  client.on('disconnect', () => {
    console.log('disconnected');
  });

  client.on('message', ({data}) => {
    userInput.close();
    userInput = null;
    if (data.sender !== 'client') {
      process.stdout.clearLine(-1);
      process.stdout.cursorTo(0);
      console.log(`${data.sender}: ${data.msg}`);
    }
    promptMessage();
  });

  client.on('error', (err) => {
    console.log('err', err);
  });

  async function promptMessage() {
    clearTimeout(promptTimeoutHandler);
    userInput = readline.createInterface(process.stdin, process.stdout);
    const msg = await getUserInput('msg: ');
    client.sendMessage(msg);
    promptTimeoutHandler = setTimeout(() => promptMessage(client), 10);
  }

  async function getUserInput(question) {
    return new Promise((resolve) => {
      userInput.question(question, (answer) => {
        resolve(answer);
        userInput.close();
      });
    });
  }
})()
