import typescript from '@rollup/plugin-typescript';
import commonjs from '@rollup/plugin-commonjs';
import dts from 'rollup-plugin-dts';
import { nodeResolve } from "@rollup/plugin-node-resolve";
import nodePolyfills from 'rollup-plugin-node-polyfills';
import json from '@rollup/plugin-json'

const config = [{
  input: `src/index.ts`,
  output: [
    {
      name: "mqttChatClient",
      file: 'dist/index.bundle.js',
      format: 'iife',
      sourcemap: 'inline',
      globals: ['mqtt', 'axios']
    }
  ],
  external: ['mqtt', 'axios'],
  watch: {
    include: 'src/**',
  },
  plugins: [
    typescript(),
    nodeResolve({browser: true}),
    nodePolyfills(),
    json(),
    commonjs({ include: 'node_modules/**'})
  ]
}, {
  input: 'dist/index.d.ts',
  output: [
    {
      file: 'dist/index.d.ts', format: 'iife'
    }
  ],
  plugins: [
    dts()
  ]
}];

export default config;
